package CommonMethods;

import EnvoirnmentAndRepository.RequestRepository;
import io.restassured.RestAssured;
import io.restassured.response.Response;
public class API_Trigger extends RequestRepository {

	static String headername = "Content-Type";
	static String headervalue = "application/json";
   
	public static Response post_API_Trigger(String reqBody, String endPoint)
    {
		Response responseBody = RestAssured.given().header(headername, headervalue).body(reqBody).post(endPoint);
 		return responseBody;
	}
    public static Response put_API_Trigger(String reqBody, String endPoint)

	{
        Response res = RestAssured.given().header(headername, headervalue).body(reqBody).put(endPoint);
		return res;
	}
    public static Response patch_API_Trigger(String reqBody, String endPoint)

	{
        Response res = RestAssured.given().header(headername, headervalue).body(reqBody).patch(endPoint);
		return res;}
    public static Response delete_API_Trigger(String endPoint)

	{
        Response res = RestAssured.given().header(headername, headervalue).delete(endPoint);
		return res;}
    public static Response get_API_Trigger(String endPoint)

	{
        Response res = RestAssured.given().header(headername, headervalue).get(endPoint);
		return res;
	}
}
