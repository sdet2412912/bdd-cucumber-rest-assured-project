package CommonMethods;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class TestNG_Retry_Analyzer implements IRetryAnalyzer {
	
	private int start=0;
	private int end=5; 
	
	public boolean retry(ITestResult result) {
		
		String TCname=result.getName();
		if(start<end) {
			System.out.println(TCname + "failed in current iteration" + start + ", hence retrying for " + (start+1));
			start++;
			return true;
		}
		return false;
	}

}
