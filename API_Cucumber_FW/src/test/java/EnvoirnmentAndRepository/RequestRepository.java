package EnvoirnmentAndRepository;

import java.io.IOException;
import java.util.ArrayList;

import CommonMethods.Utilities;

public class RequestRepository extends Envoirnment {
	
	public static String post_param_requestBody(String testCaseName) throws IOException {
		ArrayList<String> data= Utilities.readExcelData("Post_API", testCaseName);
         String reqName = data.get(1);
         String reqJob = data.get(2);
         String req_body = "{\r\n" + "\"name\": \""+reqName+"\",\r\n" + "\"job\": \""+reqJob+"\"\r\n" + "}";
         return req_body;
		
	}

	public static String post_requestBody() {

		String req_body = "{\r\n" + "\"name\": \"morpheus\",\r\n" + "\"job\": \"leader\"\r\n" + "}";
        return req_body;
	}

	public static String put_requestBody() {
		String req_Body = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n" + "}";
		return req_Body;

	}

	public static String patch_requestBody() {
		String req_Body = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n" + "}";
		return req_Body;

	}

	public static String delete_requestBody() {
		String req_Body = "";
		return req_Body;
	}

	public static String get_requestBody() {
		String req_Body = "";
		return req_Body;
	}

}
