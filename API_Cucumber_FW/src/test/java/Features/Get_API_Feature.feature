Feature: retrive the data get api.
    
@GetAPI
Scenario: retrive the data in get api with pass the endpoint.
    Given The Get API endpoint is to get user details
    When Pass the endpoint.
    Then Validate statuscode in get api is 200. 
    And Validate the responseBody in get api in list of data.
