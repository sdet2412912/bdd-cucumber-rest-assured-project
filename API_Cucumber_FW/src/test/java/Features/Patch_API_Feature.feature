Feature: Trigger the patch api with valid parameter
  
 @PatchAPI
Scenario: trigger the patch api with valid requested parameter
    Given Enter the valid Name and Job in the requestBody.
    When Send the request into the body payload
    Then Validate statuscode.
    And Validate the responseBody

@PatchAPI
Scenario Outline: trigger the patch api with valid requested parameter
    Given Enter the valid <"Name"> and <"Job"> in the requestBody.
    When Send the request into the body payload
    Then Validate statuscode.
    And Validate the responseBody

Examples:
|Name|Job|
|Nitin|QA|
|Ram|Lead|
|Kiran|PO|
    