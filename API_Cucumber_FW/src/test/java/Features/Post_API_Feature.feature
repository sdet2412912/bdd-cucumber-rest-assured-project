Feature: Trigger the post API with requested parameters 

@PostAPI
Scenario: Trigger the API request with valid request parameters 
     Given Enter NAME and JOB in request Body 
	 When Send the request with payload 
	 Then Validate the status code 
	 And Validate the response Body parameters

@PostAPI1 
Scenario Outline: Trigger the API with multiple data set
     Given Enter "<NAME>" and "<JOB>" in request Body 
	 When Send the request with payload 
	 Then Validate the status code 
	 And Validate the response Body parameters
	 
Examples:
    |NAME|JOB|
    |Nikita|QA|
    |Nilesh|Dev|
    |Niki|Tester|	 
	 	 
	 