package stepDefination;

import org.testng.Assert;

import CommonMethods.API_Trigger;
import EnvoirnmentAndRepository.Envoirnment;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;

public class Delete_API_StepDefination {
	 String reqBody;
	 String endPoint;
	 Response res;
	@Given("In Delete API delete the data into the database.")
	public void in_delete_api_delete_the_data_into_the_database() {
		endPoint = Envoirnment.delete_endpoint(); 
		
	   	}
	@When("Send the endpoint.")
	public void send_the_endpoint() {
		res = API_Trigger.delete_API_Trigger(endPoint);
	}
	@Then("Validate the status code.")
	public void validate_the_status_code() {
		int statuscode = res.statusCode();

		Assert.assertEquals(statuscode, 204, "Correct status code not found even after retrying for 5 times");

	   }
	}

