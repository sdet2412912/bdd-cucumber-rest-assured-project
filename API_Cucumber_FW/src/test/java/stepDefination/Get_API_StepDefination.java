package stepDefination;

import java.util.List;

import org.testng.Assert;

import CommonMethods.API_Trigger;
import EnvoirnmentAndRepository.Envoirnment;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Get_API_StepDefination {
	Response res;
	String endPoint;
	
	@Given("The Get API endpoint is to get user details")
	public void the_get_api_endpoint_is_to_get_user_details() {
		endPoint = Envoirnment.get_endpoint();
	    	}
	@When("Pass the endpoint.")
	public void pass_the_endpoint() {
		res=API_Trigger.get_API_Trigger(endPoint);
		
	  }
	@Then("Validate statuscode in get api is {int}.")
	public void validate_statuscode_in_get_api_is(Integer int1) {
		int statuscode=res.statusCode();
		Assert.assertEquals(statuscode, 200, "correct statuscode  and even after retrying for 5 time");	
	   }
	@Then("Validate the responseBody in get api in list of data.")
	public void validate_the_response_body_in_get_api_in_list_of_data() {
		
		ResponseBody responseBody = res.getBody();
		int exp_page = 2;
		int exp_per_page = 6;
		int exp_total = 12;
		int exp_total_pages = 2;

		int id[] = { 7, 8, 9, 10, 11, 12 };
		String email[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		String first_name[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String last_name[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
		String avatar[] = { "https://reqres.in/img/faces/7-image.jpg", "https://reqres.in/img/faces/8-image.jpg",
				"https://reqres.in/img/faces/9-image.jpg", "https://reqres.in/img/faces/10-image.jpg",
				"https://reqres.in/img/faces/11-image.jpg", "https://reqres.in/img/faces/12-image.jpg" };

		int res_page = responseBody.jsonPath().getInt("page");
		int res_per_page = responseBody.jsonPath().getInt("per_page");
		int res_total = responseBody.jsonPath().getInt("total");
		int res_total_pages = responseBody.jsonPath().getInt("total_pages");

		List<String> dataArray = responseBody.jsonPath().getList("data");
		int sizeofarray = dataArray.size();

		Assert.assertEquals(res_page, exp_page);
		Assert.assertEquals(res_per_page, exp_per_page);
		Assert.assertEquals(res_total, exp_total);
		Assert.assertEquals(res_total_pages, exp_total_pages);

		for (int i = 0; i < sizeofarray; i++) {
			Assert.assertEquals(Integer.parseInt(responseBody.jsonPath().getString("data[" + i + "].id")), id[i],
					"Validation of id failed " + i);
			Assert.assertEquals(responseBody.jsonPath().getString("data[" + i + "].email"), email[i],
					"Validation of email failed" + i);
			Assert.assertEquals(responseBody.jsonPath().getString("data[" + i + "].first_name"), first_name[i],
					"Validation of email failed" + i);
			Assert.assertEquals(responseBody.jsonPath().getString("data[" + i + "].last_name"), last_name[i],
					"Validation of email failed " + i);
			Assert.assertEquals(responseBody.jsonPath().getString("data[" + i + "].avatar"), avatar[i],
					"Validation of email failed" + i);

		}
		String exp_url = "https://reqres.in/#support-heading";
		String exp_text = "To keep ReqRes free, contributions towards server costs are appreciated!";

		Assert.assertEquals(responseBody.jsonPath().getString("support.url"), exp_url, "Validation of URL failed");
		Assert.assertEquals(responseBody.jsonPath().getString("support.text"), exp_text, "Validation of text failed");


	}
}