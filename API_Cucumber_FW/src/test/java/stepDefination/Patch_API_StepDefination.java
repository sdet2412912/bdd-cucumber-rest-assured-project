package stepDefination;

import java.time.LocalDateTime;

import org.testng.Assert;

import CommonMethods.API_Trigger;
import EnvoirnmentAndRepository.Envoirnment;
import EnvoirnmentAndRepository.RequestRepository;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Patch_API_StepDefination {
	
	String endpoint;
	String req_body;
	Response res;
	ResponseBody responseBody;
	
	@Given("Enter the valid <{string}> and <{string}> in the requestBody.")
	public void enter_the_valid_and_in_the_request_body(String rname, String rjob) {
		req_body = "{\r\n" + "\"name\": \""+rname+"\",\r\n" + "\"job\": \""+rjob+"\"\r\n" + "}";
		endpoint = Envoirnment.patch_endpoint();}

	@Given("Enter the valid Name and Job in the requestBody.")
	public void enter_the_valid_name_and_job_in_the_request_body() {
		req_body = RequestRepository.patch_requestBody();
		endpoint = Envoirnment.patch_endpoint();
	
	}
	   
	@When("Send the request into the body payload")
	public void send_the_request_into_the_body_payload() {
		res=API_Trigger.patch_API_Trigger(req_body, endpoint);
	    
	}
	    
	@Then("Validate statuscode.")
	public void validate_statuscode() {
		int statuscode=res.statusCode();
		 Assert.assertEquals(statuscode, 200, "Correct statuscode not found even after retrying for 5 times");

	}
	   
	@Then("Validate the responseBody")
	public void validate_the_response_body() {
		responseBody = res.getBody();

		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_updatedAt = responseBody.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.toString().substring(0, 11);

		JsonPath jsp_req = new JsonPath(req_body);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		Assert.assertEquals(res_name, req_name, "Name in Res_Body is not equal to Name sent in Res_Body");
		Assert.assertEquals(res_job, req_job, "Job in Res_Body is not equal to Job sent in Res_Body ");
		
		Assert.assertEquals(res_updatedAt, expecteddate, "createdAt in Res_Body is not equal to Date Generated");
	}

}
