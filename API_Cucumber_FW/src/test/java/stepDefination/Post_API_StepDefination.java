package stepDefination;

import java.time.LocalDateTime;

import org.junit.Test;
import org.testng.Assert;

import CommonMethods.API_Trigger;
import CommonMethods.TestNG_Retry_Analyzer;
import EnvoirnmentAndRepository.Envoirnment;
import EnvoirnmentAndRepository.RequestRepository;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import restAssured.Post_API_Trigger_Reference;

public class Post_API_StepDefination {

	String endpoint;
	String req_body;
	Response res;
	ResponseBody responseBody;

	@Given("Enter NAME and JOB in request Body")
	public void enter_name_and_job_in_post_request_body() {

		req_body = RequestRepository.post_requestBody();
		endpoint = Envoirnment.post_endpoint();
	}

	@Given("Enter {string} and {string} in request Body")
	public void enter_and_in_request_post_body(String reqname, String reqjob) {
		req_body = "{\r\n" + "\"name\": \""+reqname+"\",\r\n" + "\"job\": \""+reqjob+"\"\r\n" + "}";
		endpoint = Envoirnment.post_endpoint();
	}
	   
	@When("Send the request with payload")
	public void send_the_request_with_payload() {
		res = API_Trigger.post_API_Trigger(req_body, endpoint);
	}

	@Then("Validate the status code")
	public void validate_the_status_code() {
		int statuscode=res.statusCode();
		 Assert.assertEquals(statuscode, 201, "Correct statuscode not found even after retrying for 5 times");
		 
	}

	@Then("Validate the response Body parameters")
	public void validate_the_response_body_parameters() {

		responseBody = res.getBody();

		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_id = responseBody.jsonPath().getString("id");
		String res_createdAt = responseBody.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.toString().substring(0, 11);

		JsonPath jsp_req = new JsonPath(req_body);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		Assert.assertEquals(res_name, req_name, "Name in Res_Body is not equal to Name sent in Res_Body");
		Assert.assertEquals(res_job, req_job, "Job in Res_Body is not equal to Job sent in Res_Body ");
		Assert.assertNotNull(res_id, "Id in Res_Body is found to be null");
		Assert.assertEquals(res_createdAt, expecteddate, "createdAt in Res_Body is not equal to Date Generated");

	}

}
