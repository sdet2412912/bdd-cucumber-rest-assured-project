package stepDefination;

import java.time.LocalDateTime;

import org.testng.Assert;

import CommonMethods.API_Trigger;
import EnvoirnmentAndRepository.Envoirnment;
import EnvoirnmentAndRepository.RequestRepository;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Put_API_StepDefination {
	String endpoint;
	String req_body;
	int statuscode;
	Response res;
	ResponseBody responseBody;
	@Given("Enter the valid Name and Job in requestBody.")
	public void enter_the_valid_name_and_job_in_request_body() {
	  
		req_body = RequestRepository.put_requestBody();
		endpoint = Envoirnment.put_endpoint();
	}

	@Given("Enter the valid <{string}> and <{string}> in requestBody.")
	public void enter_the_valid_and_in_request_body(String reqname, String reqjob) {
	   	req_body = "{\r\n" + "\"name\": \""+reqname+"\",\r\n" + "\"job\": \""+reqjob+"\"\r\n" + "}";
		endpoint = Envoirnment.put_endpoint();
	}
@When("Send the request in body payload.")
	public void send_the_request_in_body_payload() {
		res=API_Trigger.put_API_Trigger(req_body, endpoint);
	    	}
	@Then("validate statuscode.")
	public void validate_statuscode() {
		 statuscode=res.statusCode();
		 Assert.assertEquals(statuscode, 200, "Correct statuscode not found even after retrying for 5 times");

	    	}
	@Then("validate the responseBody.")
	public void validate_the_response_body() {
		responseBody = res.getBody();

		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_updatedAt = responseBody.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.toString().substring(0, 11);

		JsonPath jsp_req = new JsonPath(req_body);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		Assert.assertEquals(res_name, req_name, "Name in Res_Body is not equal to Name sent in Res_Body");
		Assert.assertEquals(res_job, req_job, "Job in Res_Body is not equal to Job sent in Res_Body ");
		
		Assert.assertEquals(res_updatedAt, expecteddate, "createdAt in Res_Body is not equal to Date Generated");


	  	}


	
		   }




