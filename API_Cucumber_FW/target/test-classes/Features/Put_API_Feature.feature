Feature: Trigger the put api with valid parameter.
    
@PutAPI    
Scenario: trigger the api with valid requested parameter.
    Given Enter the valid Name and Job in requestBody.
    When Send the request in body payload.
    Then validate statuscode. 
    And validate the responseBody.
@PutAPI1    
Scenario Outline: trigger the api with valid requested parameter.
    Given Enter the valid <"Name"> and <"Job"> in requestBody.
    When Send the request in body payload.
    Then validate statuscode. 
    And validate the responseBody.



Examples:
    |Name|Job|
    |Nikhil|lead|
    |Nilesh|Dev|
    |Niki|Manager|	 
    
    